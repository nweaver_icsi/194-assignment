#!/usr/bin/env python

import socket
import struct
import os
import traceback
import time
import sys
import subprocess
import shlex
import threading
import subprocess

import NWDNS

def persistent_log(msg):
    print msg


mrec = None
mdns_sock = None

mdns_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
mdns_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mdns_sock.bind(('', 5353))

mreq = struct.pack("=4sl", socket.inet_aton("224.0.0.251"),
                   socket.INADDR_ANY)
mdns_sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)




loglist = []

logSemaphore = threading.Semaphore()

ip_local = "Not Found"
ip_global = "Not Found"
enet = "Not Found"
version = "Not Found"


def log(msg):
    logSemaphore.acquire()
    t = time.strftime('%Y-%m-%d %H:%M:%S ')
    threadname = "XXX"
    try:
        threadname = threading.currentThread().getName()
    except:
        pass
    while len(threadname) < 10:
        threadname += " "

    for line in msg.split('\n'):
        res = '%s %s %s' % (t, threadname, line.strip())
        sys.stdout.write('%s\n' % res)
        loglist.insert(0, res)
    logSemaphore.release()
    sys.stdout.flush()

def log_exception():
    log(traceback.format_exc())

def add_config(c):
    for line in c:
        line = line.split("#")[0]
        line = line.split(":", 1)
        if len(line) > 1:
            config[line[0].strip()] = line[1].strip()


def load_config(file):
    c = open(file)
    add_config(c)

def update_internal():
    persistent_log("Starting update process")
    log("Starting update")
    infile = get_command_fd('update')
    log("Got update file")
    outfile = open('/tmp/update.tar.gz', 'w')
    for line in infile:
        outfile.write(line)
    outfile.close()
    log("Unpacking the tarball")

    local_cmd = ['tar', '-xvzf', 
                 '/tmp/update.tar.gz', 
                 '-C',
                 '/root/plug_code']
    try:
        log("Tar command %s" % local_cmd)
        for line in subprocess.Popen(local_cmd, stdout=subprocess.PIPE).stdout:
            log("TAR: %s" % line.strip())

        log("TAR complete, running update.sh")
        log_thread = threading.Thread(target=upload_log_delay, name="LogUpload")
        log_thread.start()
        for line in subprocess.Popen(['/root/plug_code/run_update.sh'],
                                     stdout=subprocess.PIPE).stdout:
            log("UPDATER: %s" % line.strip())
    
    except:
        log_exception()
        log("Update command failed")
        return

    log("Update command complete")


def upload_log_delay():
    time.sleep(20)
    upload_log()



def update(addr, socket):
    socket.sendto("Starting update command", addr)
    update_internal()


def lock_update(addr, socket):
    try:
        f = open('UPDATE.LOCK', 'w')
        f.write('')
        f.close()
        socket.sendto("ACK: lock-update", addr)
        log("lock-update successful")
    except:
        log_exception()
        socket.sendto("ACK: lock-update failed", addr)
        log("lock-update failed");

def unlock_update(addr, socket):
    try:
        os.remove('UPDATE.LOCK')
        socket.sendto("ACK: unlock-update", addr)
        log("unlock-update successful")
    except:
        log_exception()
        socket.sendto("ACK: unlock-update failed", addr)
        log("unlock-update failed")


def hardlock_update(addr, socket):
    try:
        f = open('HARDLOCK.LOCK', 'w')
        f.write('')
        f.close()
        socket.sendto("ACK: hardlock-update", addr)
        log("hardlock-update successful")
    except:
        log_exception()
        socket.sendto("ACK: hardlock-update failed", addr)
        log("hardlock-update failed")

def full_reset(addr, socket):
    socket.sendto("ACK: beginning full-reset", addr)
    command = "%s/main.sh full-reset &" % sys.argv[1]
    log(command)
    command = shlex.split(command)
    p = subprocess.Popen(command)


def stop(addr, socket):
    socket.sendto("ACK: beginning stop", addr)
    command = "%s/main.sh stop &" % sys.argv[1]
    log(command)
    command = shlex.split(command)
    p = subprocess.Popen(command)


def reset(addr, socket):
    socket.sendto("ACK: beginning reset", addr)
    command = "%s/main.sh restart &" % sys.argv[1]
    log(command)
    command = shlex.split(command)
    p = subprocess.Popen(command)


def restart(addr, socket):
    socket.sendto("ACK: beginning system reboot", addr)
    command = "/sbin/shutdown -r now 'Automatic Reset'"
    log(command)
    command = shlex.split(command)
    p = subprocess.Popen(command)


def get_config(sock):
    msg = ""
    for item in config:
        msg += "%s: %s\n" % (item, config[item])
    sock.sendall(msg)
    log("Config successful")

def tcp_log(sock):
    msg = ""
    for line in loglist:
        msg = line + '\n' + msg
    sock.sendall(msg)
    log("TCP log successful")

def start_log(sock):
    msg = ""
    file = open('/tmp/start.log', 'r')
    for line in file:
        msg += line
    sock.sendall(msg)
    log("TCP log successful")

def file_log(sock):
    msg = ""
    file = open('/tmp/multicast.log', 'r')
    for line in file:
        msg += line
    sock.sendall(msg)
    log("TCP log successful")

def mcast_log(addr, socket):
    msg = ""
    for line in loglist:
        if len(msg) + len(line) > 1400:
            break
        msg = line + '\n' + msg
    socket.sendto(msg, addr)
    log("mcast-log successful")

def tcp_sock(addr, socket):
    if tcp_socket != None:
        socket.sendto("%i" % tcp_socket_port, addr)
    else:
        socket.sendto("None" % tcp_socket_port, addr)
    log("tcp-sock successful")

def get_ip(addr, socket):
    msg = "IP Local: %s\nIP Global: %s\n" % (ip_local, ip_global)
    socket.sendto(msg, addr)
    log("get-ip successful")

def respond(data, addr, socket):
    cmd = data.split()[0]
    serial = ''
    log("Command %s" % data.strip())
    if len(data.split(' ', 1)) > 1:
        serial = data.split(' ', 1)[1]
    if cmd == 'key':
        socket.sendto(key, addr)
        
    elif serial == myserial:
        if cmd == 'reset':
            return reset(addr, socket)
        if cmd == 'restart':
            return restart(addr, socket)
        elif cmd == 'lock-update':
            return lock_update(addr, socket)
        elif cmd == 'update':
            return update(addr, socket)
        elif cmd == 'unlock-update':
            return unlock_update(addr, socket)
        elif cmd == 'hardlock-update':
            return hardlock_update(addr, socket)
        elif cmd == 'full-reset':
            return full_reset(addr, socket)
        elif cmd == 'stop':
            return stop(addr, socket)
        elif cmd == 'mcast-log':
            mcast_log(addr, socket)
        elif cmd == 'tcp-sock':
            tcp_sock(addr, socket)

        elif cmd == 'ip':
            get_ip(addr, socket)
            
        else:
            socket.sendto("Unknown command", addr)


# The SSH client program wants each SSH as a unique connection,
# But thats fine, really
def get_command_fd(command):
    ssh_cmd = "/usr/bin/ssh -o StrictHostKeyChecking=no -p %s %s@%s" % \
        (config['controller-port'],
         config['username'],
         config['controller'])
    args = shlex.split(ssh_cmd)
    p = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    log("Querying for remote command \"%s %s\"" % (command, myserial))
    # log("SSH command is %s" % ssh_cmd)
    p.stdin.write('%s %s\n' % (command, myserial))
    p.stdin.close()
    return p.stdout


def get_command(command):
    fd = get_command_fd(command)
    log("Got SSH connection")
    res = ""
    for line in fd:
        res += line
    sys.stderr.write("Response:")
    sys.stderr.write(res)
    sys.stderr.flush()
    return res

def get_ip_info():
    global ip_local, ip_global, enet, version

    local_cmd = ['/sbin/ifconfig', 'em1']
    for line in subprocess.Popen(local_cmd, stdout=subprocess.PIPE).stdout:
        data = line.split()
        if len(data) > 0 and data[0] == 'inet':
            ip_local = data[1].split(':')[1]
        if len(data) > 4 and data[3] == 'HWaddr':
            enet = data[4]
    print ip_local, enet


plug_name = "nsa.local"
plug_index = 1

def get_plug_name():
    global plug_name
    global plug_index
    log("Seeing if there are any other plugs on this network")
    log("With the name %s" % plug_name)
    question = NWDNS.DNSMessage()
    question.header.qr = False
    question.question[0] = NWDNS.DNSQuestion()
    question.question[0].qname = plug_name
    question.question[0].qtype = NWDNS.RTYPE_A

    mdns_sock.settimeout(1)

    foundPlug = False

    for x in range(3):
        if foundPlug:
            break
        log("Sending MDNS question")
        mdns_sock.sendto(question.pack(),
                         ('224.0.0.251',
                          5353))
        try:
            for y in range(10):
                if foundPlug:
                    break
                data, addr = mdns_sock.recvfrom(1024)
                msg = NWDNS.DNSMessage(data)
                for answer in msg.answer:
                    log("MDNS answer %s" % answer.shortRepr())
                    if answer.name.lower() == plug_name:
                        log("Another plug with my name is on the net")
                        foundPlug = True
        except:
            pass

    if foundPlug:
        plug_index += 1
        plug_name = "nsa%i.local" % plug_index
        get_plug_name()




def mdns_loop():
    log("Multicast DNS responder starting")
    get_plug_name()
    log("My name is %s" % plug_name)

    mdns_sock.settimeout(None)
    while True:
        try:
            data, addr = mdns_sock.recvfrom(1024)
            msg = NWDNS.DNSMessage(data)
            for x in range(len(msg.question)):
                if msg.question[x].qname.lower() == plug_name:
                    log("Got request for plug.local, RYTPE = %s" %
                        NWDNS.rtypeName(msg.question[0].qtype))
                    log("Rechecing my IP")
                    get_ip_info()
                    answer = NWDNS.DNSMessage()
                    answer.header.rcode = NWDNS.RCODE_OK
                    answer.header.qr = True
                    answer.header.aa = True
                    answer.header.id = msg.header.id
                    answer.header.opcode = msg.header.opcode
                    answer.question = []
                    if msg.question[x].qtype == NWDNS.RTYPE_A or \
                            msg.question[x].qtype == NWDNS.RTYPE_ANY:
                        answer.answer.append(NWDNS.DNSAnswer(
                                msg.question[x].qname,
                                NWDNS.RTYPE_A,
                                ip_local, 10));
                    
                    if len(answer.answer) > 0:
                        log("Generating multicast response")
                        mdns_sock.sendto(answer.pack(),
                                          ('224.0.0.251', 5353))

        except KeyboardInterrupt:
            log("Got Ctrl-C, exiting")
            sys.exit(0)
        except:
            log("Got exception!")
            log_exception()



def get_my_ip():
    try:
        
        host = socket.gethostbyname(config['echo'])
        port = 443
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(1.0)
        s.connect((host, port))
        data = s.recv(1024)
        return data.split(":")[0]
    except:
        pass
    return ""





if __name__ == '__main__':
    log("Starting multicast responder")
    get_ip_info()
    mdns_thread = threading.Thread(target=mdns_loop, name="mDNS")
    mdns_thread.start()

